<?php
defined('_JEXEC') or die;
for ($i=1;$i<=10;$i++) {
  ${'image'.$i} = $params->get('image'.$i);
  ${'slide'.$i.'Heading'} = $params->get('slide'.$i.'_heading');
  ${'destination'.$i} = $params->get('destination'.$i);
  ${'descriptionFirst'.$i} = $params->get('descriptionFirst'.$i);
  ${'descriptionSecond'.$i} = $params->get('descriptionSecond'.$i);
  ${'link'.$i} = $params->get('link'.$i);
  ${'titleLink'.$i} = $params->get('titleLink'.$i);
}

JHTML::script('modules/mod_slider/js/jMyCarousel.js');
JHTML::stylesheet('modules/mod_slider/css/style.css');
?>
<script type="text/javascript">
  jQuery(function() {
      jQuery(".jMyCarousel").jMyCarousel({
          visible: '100%'
      });
  });
</script>

<div class="jMyCarousel">
  <ul>
    <?php
    for ($i=1;$i<=10;$i++){
      if(${'image'.$i} != "") {
    ?>
    <li>
      <img src="<?php echo ${'image'.$i} ?>" width="200" height="150">
      <div style="overflow: hidden;" class="fond"></div>
      <h3 class="titre"><?php echo ${'slide'.$i.'Heading'} ?></h3>
      <p style="padding: 2px 8px;"><?php echo ${'destination'.$i} ?></p>
      <p style="padding: 2px 8px;"><?php echo ${'descriptionFirst'.$i} ?></p>
      <p style="padding: 2px 8px;"><?php echo ${'descriptionSecond'.$i} ?></p>
      <p style="padding: 2px 8px;"><a href="index.php?option=com_content&view=article&id=<?php echo ${'link'.$i}?>"><?php echo ${'titleLink'.$i} ?></a></p>
    </li>
    <?php
    }}
    ?>
  </ul>
</div>
